let lat;
let lon;




//Une fois la page html chargé on demande l'accès à la position de l'utilisateur

document.addEventListener("DOMContentLoaded", function () {
    navigator.geolocation.getCurrentPosition(allow, reject);

});


//fonction qui s'execute si l'utilisateur accepte de partager sa position
//puis on initialise la carte avec la fonction initMap
function allow(position) {
    lat = position.coords.latitude;
    lon = position.coords.longitude;
    initmap();
}


//fonction qui s'execute si l'utilisateur refuse de partager sa position

function reject(error) {
    let message = "";
    switch (error.code) {
        case 1:
            message = "Permission refusée";
            break;
        case 2:
            message = "Position non disponible";
            break;
        case 3:
            message = "Dépassement de délai";
            break;
        case 4:
            message = "Erreur inconnue";
            break;
    }
    console.log(message);
}




//fonction qui sert à enlever les numéros au nom des arrêts
function cut(sentence) {
    let tab = sentence.split(' ');
    let retour = "";

    for (const i in tab) {
        if (i > 0) {
            retour = retour + " " + tab[i];
        }
    }
    return retour;
}


// initialisation d'un icone pour les station de vélo
var yeloIcon = new L.Icon({
    iconUrl: "./svg/bike.svg",
    iconSize: [40, 30],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41],
    className: 'velo',
});


// initialisation d'un icone pour les bus
var busIcon = new L.Icon({
    iconUrl: "https://gizmobrewworks.com/wp-content/uploads/leaflet-maps-marker-icons/busstop.png",
    iconSize: [40, 40],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41],
    className: 'bus',
});


// Initialisation d'un icone pour la gare de la rochelle 

var redIcon = new L.Icon({
    iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41],
    className: 'train',
});


//Initialisation d'un icone pour la position de l'utilisateur
var user = L.divIcon({
    html: '<i class="fa-solid fa-location-arrow fa-2x"></i>',
    iconSize: [20, 20],
    className: 'user'
});


// fonction qui permet d'afficher les horraires des bus passant à l'arrêt donner en paramètre 

/*

    premièrement on parcourt à l'aide du fetch les données de l'api qui regroupe les horraires des bus pour un arrêt donné
    puis on stock l'horraire et l'identifiant du trajet dans une liste
    Ensuite on parcourt à l'aide du fetch les numéro de bus qui passent à l'arrêt donné puis on affiche les données dans un tableau

 */
function showhorraire(arg) {

    let trip_id = [];
    let horraire = [];


    fetch("https://api.agglo-larochelle.fr/production/opendata/api/records/1.0/search/dataset=transport_yelo___gtfs_stop_times_des_bus&rows=100000&sort=trip_id&facet=trip_id&refine.stop_id=" + arg)
        .then(response => response.json())
        .then(texte => {

            for (const result of texte.records) {
                if (!trip_id.includes(result.fields.trip_id)) {
                    trip_id.push(result.fields.trip_id);
                    horraire.push(result.fields.departure_time);
                }
            }


        })
        .catch(error => {
            console.log("Erreur durant le fetch");
        }
        );


    const info_table = document.createElement("table");
    info_table.setAttribute("border", "2");
    const header_row = document.createElement("tr");
    info_table.appendChild(header_row);
    const line_title = document.createElement("th");
    line_title.innerHTML = "Numéro de ligne";
    info_table.appendChild(line_title);
    const line_direction = document.createElement("th");
    line_direction.innerHTML = "Direction";
    info_table.appendChild(line_direction);
    const time_title = document.createElement("th");
    time_title.innerHTML = "Prochain bus";
    info_table.appendChild(time_title);
    let table = document.getElementById("horraire");
    table.innerHTML = "";
    table.appendChild(info_table);




    fetch("https://api.agglo-larochelle.fr/production/opendata/api/records/1.0/search/dataset=transport_yelo___gtfs_trips_des_bus&rows=147000&sort=trip_id&facet=route_id")
        .then(response => response.json())
        .then(texte => {

            for (const result of texte.records) {
                for (i of trip_id) {
                    if (i == result.fields.trip_id) {
                        let new_tr = document.createElement("tr");
                        info_table.appendChild(new_tr);
                        let new_th_1 = document.createElement("th");
                        let new_th_2 = document.createElement("th");
                        let new_th_3 = document.createElement("th");


                        new_th_1.innerHTML = result.fields.route_id;
                        new_th_2.innerHTML = result.fields.trip_headsign;
                        new_th_3.innerHTML = horraire[trip_id.indexOf(i)];


                        info_table.appendChild(new_th_1);
                        info_table.appendChild(new_th_2);
                        info_table.appendChild(new_th_3);

                    }
                }
            }


        })
        .catch(error => {
            console.log("Erreur durant le fetch");
        }
        );

}

//fonction qui vérifie qu'un marker est bien dans la zone de la carte définit par un cercle de 10km ( rayon configurable )

function isMarkerInCircle(circle, mark) {
    let circleCenter = circle.getLatLng();
    let markerPosition = mark.getLatLng();
    return circleCenter.distanceTo(markerPosition) <= circle.getRadius();
}



let horraire_train = [];
let direction_train = [];
let id_train = [];
let type_train = [];
            

//fonction qui permet d'afficher les horraires,Direction, N° Train et Type des trains passant à la gare de la rochelle
function showhorrairetrain(){
    const info_table = document.createElement("table");
    info_table.setAttribute("border", "2");
    const header_row = document.createElement("tr");
    info_table.appendChild(header_row);

    const line_direction = document.createElement("th");
    line_direction.innerHTML = "Direction";
    info_table.appendChild(line_direction);
    const time_title = document.createElement("th");
    time_title.innerHTML = "Horraire Train";
    info_table.appendChild(time_title);

    const IdTrain = document.createElement("th");
    IdTrain.innerHTML = "N° Train";
    info_table.appendChild(IdTrain);


    const typeTrain = document.createElement("th");
    typeTrain.innerHTML = "Type Train";
    info_table.appendChild(typeTrain);





    let table = document.getElementById("horraire");
    
    table.innerHTML = "";
    table.appendChild(info_table);

    for (i in horraire_train){
        let new_tr = document.createElement("tr");
        info_table.appendChild(new_tr);
        let new_th_1 = document.createElement("th");
        let new_th_2 = document.createElement("th");
        let new_th_3 = document.createElement("th");
        let new_th_4 = document.createElement("th");

        new_th_1.innerHTML = direction_train[i];
        new_th_2.innerHTML = horraire_trainToDate(horraire_train[i]);
        new_th_3.innerHTML = id_train[i];
        new_th_4.innerHTML = type_train[i];

        info_table.appendChild(new_th_1);
        info_table.appendChild(new_th_2);
        info_table.appendChild(new_th_3);
        info_table.appendChild(new_th_4);


    }
}

//fonction qui permet de convertir les horraires des trains en date
function horraire_trainToDate(arg){


    let year = arg.slice(0,4);
    let month = arg.slice(4,6);
    let day = arg.slice(6,8);

    let hour = arg.slice(9,11);
    let minute = arg.slice(11,13);
    let second = arg.slice(13);
    return day + "/" + month + "/" + year + " " + hour + ":" + minute + ":" + second;
}

//fonction initmap qui permet d'initialiser la carte en affichant les marker au bon endroit 
function initmap() {
    let all_velo = L.layerGroup();
    let all_bus = L.layerGroup();
    let all_train = L.layerGroup();



    var circle = L.circle([lat, lon], {
        color: 'red',
        fillColor: '#f03',
        fillOpacity: 0.1,
        radius: 10000
    });

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    fetch("https://api.agglo-larochelle.fr/production/opendata/api/records/1.0/search/dataset=yelo___disponibilite_des_velos_en_libre_service&facet=station_nom&api-key=bc662169-c735-4026-be45-754b6c797d93")
        .then(response => response.json())
        .then(texte => {
            for (const result of texte.records) {

                let marker = L.marker([result.fields.station_latitude, result.fields.station_longitude], { icon: yeloIcon }).bindPopup("<b>Nom : " + cut(result.fields.station_nom) + "</b><br>" + "<b>Vélos disponibles : " + result.fields.velos_disponibles + " / " + result.fields.nombre_emplacements + "</b><br>").openPopup();
                if (isMarkerInCircle(circle, marker)) {
                    all_velo.addLayer(marker);

                }



            }
            
        })
        .catch(error => {
            document.body.innerText = "Erreur durant le fetch :" + error;
        }
        );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    fetch("https://api.agglo-larochelle.fr/production/opendata/api/records/1.0/search/dataset=transport_yelo___gtfs_stop_des_bus&rows=1517")
        .then(response => response.json())
        .then(texte => {


            for (const result of texte.records) {

                let marker = L.marker([result.fields.stop_lat, result.fields.stop_lon],{icon: busIcon}).on('click', function () { showhorraire(result.fields.stop_id) }).bindPopup("<b>Nom : " + result.fields.stop_name + "</b><br>").openPopup()
                if (isMarkerInCircle(circle, marker)) {
                    all_bus.addLayer(marker);
                }


            }
            
        })
        .catch(error => {
            document.body.innerText = "Erreur durant le fetch :" + error;
        }
        );


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
fetch("https://api.sncf.com/v1/coverage/sncf/stop_areas/stop_area%3ASNCF%3A87485003/departures?", {
        headers: {
            "Authorization": "7ec1e5e3-68d9-4fc1-91ee-9657e51568a5"
        }
    })
        .then(response => response.json())
        .then(texte => {
            for (const result of texte.departures) {
                let marker = L.marker([result.stop_point.coord.lat, result.stop_point.coord.lon],{ icon: redIcon }).on('click', function () { showhorrairetrain() }).bindPopup("<b>Nom : Gare de La Rochelle" + "</b><br>").openPopup();
                //if (isMarkerInCircle(circle, marker)) {
                    all_train.addLayer(marker);
                    horraire_train.push(result.stop_date_time.departure_date_time);
                    direction_train.push(result.display_informations.direction);
                    id_train.push(result.display_informations.headsign);
                    type_train.push(result.display_informations.network);
              //  }
            }

        })
        .catch(error => {});


/*
Gestion des layout de la carte permettant de choisir les layers à afficher ou à cacher .
*/
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    var osm = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '© OpenStreetMap'
    });
    let map = L.map('map', {
        center: [lat, lon],
        zoom: 15,
        layers: [osm, all_bus, all_velo]
    }
    );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    var legend = L.control({position: 'bottomleft'});
legend.onAdd = function (map) {

var div = L.DomUtil.create('div', 'legend');
    div.innerHTML ="Legende : <br> <img src='https://gizmobrewworks.com/wp-content/uploads/leaflet-maps-marker-icons/busstop.png' width='20px' height='20px'> : Arrêt de Bus <br> <img src='./svg/bike.svg' width='20' height='20'> : Station de vélo <br> <img src='https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-red.png' width='20' height='20'> : Gare <br>";
return div;
};
legend.addTo(map);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    


    circle.addTo(map);
    var baseMaps = {
        "OpenStreetMap": osm,
    };

    var velo = {
        "Vélo": all_velo
    };

    let layerControl = L.control.layers(baseMaps, velo).addTo(map);
    layerControl.addOverlay(all_bus, "Bus").addTo(map);
    layerControl.addOverlay(all_train, "Train").addTo(map);

    L.marker([lat,lon],{icon : user}).addTo(map).bindPopup("<b>Vous êtes ici</b>").openPopup();
    

}

